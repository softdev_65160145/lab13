/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.model.Customer;
import java.util.List;

/**
 *
 * @author jjack46
 */
public class CustomerService {

    public List<Customer> getCustomers() {
        CustomerDao userDao = new CustomerDao();
        return userDao.getAll();
    }

    public Customer addNew(Customer editedUser) {
        CustomerDao userDao = new CustomerDao();
        return userDao.save(editedUser);
    }

    public Customer updateCustomer(Customer editedUser) {
        CustomerDao userDao = new CustomerDao();
        return userDao.update(editedUser);
    }

    public int delete(Customer editedUser) {
        CustomerDao userDao = new CustomerDao();
        return userDao.delete(editedUser);
    }
}
